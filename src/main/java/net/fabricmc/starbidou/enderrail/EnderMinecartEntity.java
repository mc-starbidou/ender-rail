package net.fabricmc.starbidou.enderrail;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.vehicle.AbstractMinecartEntity;
import net.minecraft.entity.vehicle.MinecartEntity;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvent;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class EnderMinecartEntity extends MinecartEntity {

    private boolean firstTick = true;
    public EnderMinecartEntity(EntityType<? extends EnderMinecartEntity> entityType, World world) {
        super(entityType, world);
    }

    public EnderMinecartEntity(World world, double x, double y, double z) {
        this(EnderRailMod.ENDER_MINECART, world);
        this.setPosition(x, y, z);
        this.prevX = x;
        this.prevY = y;
        this.prevZ = z;
    }

    @Override
    public void dropItems(DamageSource damageSource) {
        // Drop nothing
    }

    @Override
    public void tick() {
        super.tick();
        if( firstTick )
        {
            firstTick = false;
        }
        else if (!this.hasPassengers()) {
            world.playSound(null, getX(), getY(), getZ(), SoundEvents.ENTITY_ENDERMAN_TELEPORT, SoundCategory.BLOCKS, 1f, 1f);
            this.kill();
        }
    }

}
