package net.fabricmc.starbidou.enderrail;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.blockrenderlayer.v1.BlockRenderLayerMap;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricDefaultAttributeRegistry;
import net.minecraft.block.*;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnGroup;
import net.minecraft.entity.vehicle.MinecartEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.Items;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EnderRailMod implements ModInitializer {

	public static final Logger LOGGER = LoggerFactory.getLogger("starbidous_ender_rail");

	public static final Block ENDER_RAIL_BLOCK = Registry.register(Registry.BLOCK, new Identifier("starbidous_ender_rail", "ender_rail"), new EnderRailBlock(AbstractBlock.Settings.of(Material.DECORATION).noCollision().strength(0.7f).sounds(BlockSoundGroup.METAL)));
	public static final Item ENDER_RAIL_ITEM = Items.register(ENDER_RAIL_BLOCK, ItemGroup.TRANSPORTATION);

	public static final EntityType<EnderMinecartEntity> ENDER_MINECART =  registerEntity("starbidous_ender_rail:ender_minecart", EntityType.Builder.create((EntityType<EnderMinecartEntity> entityType, World world) -> new EnderMinecartEntity(entityType, world), SpawnGroup.MISC).setDimensions(0.98f, 0.7f).maxTrackingRange(8));
	@Override
	public void onInitialize() {
		BlockRenderLayerMap.INSTANCE.putBlock(ENDER_RAIL_BLOCK, RenderLayer.getCutout());
	}

	private static <T extends Entity> EntityType<T> registerEntity(String id, EntityType.Builder<T> type) {
		return Registry.register(Registry.ENTITY_TYPE, id, type.build(id));
	}
}