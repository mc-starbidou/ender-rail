package net.fabricmc.starbidou.enderrail;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.rendering.v1.EntityModelLayerRegistry;
import net.fabricmc.fabric.api.client.rendering.v1.EntityRendererRegistry;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricDefaultAttributeRegistry;
import net.minecraft.client.render.entity.model.EntityModelLayer;
import net.minecraft.util.Identifier;

@Environment(EnvType.CLIENT)
public class ClientInitializer implements ClientModInitializer {

    public static final EntityModelLayer ENDER_MINECART_LAYER = new EntityModelLayer(new Identifier("starbidous_ender_rail", "ender_minecart"), "main");
    @Override
    public void onInitializeClient() {
        EntityRendererRegistry.register(EnderRailMod.ENDER_MINECART, (context) -> {
            return new EnderMinecartRenderer(context, ENDER_MINECART_LAYER);
        });

        EntityModelLayerRegistry.registerModelLayer(ENDER_MINECART_LAYER, EnderMinecartModel::getTexturedModelData);
    }
}
